import java.util.LinkedList;

class Circle{
    public double r;
    public double s;

    public Circle(double r, double s) {
        this.r = r;
        this.s = s;
    }
}

class MinMax{
    public int min;
    public int max;

    public MinMax(int min, int max){
        this.min = min;
        this.max = max;
    }
}

class T{
    public double a;
    public double R1;
    public double R2;
    public double s;

    public T(double a, double R1, double R2, double s){
        this.a = a;
        this.R1 = R1;
        this.R2 = R2;
        this.s = s;
    }
}

class Pow2{
    public int N;
    public int sum[];

    public Pow2(int n, int[] sum) {
        N = n;
        this.sum = sum;
    }
}

class Sum{
    int K;
    double sum;

    public Sum(int k, double sum) {
        K = k;
        this.sum = sum;
    }
}

class Array{
    public int count;
    public LinkedList<Integer> index;

    public Array(int count, LinkedList<Integer> index) {
        this.count = count;
        this.index = index;
    }
}

class Matrix{
    public int M;
    public int N;
    public int[][] m;

    public Matrix(int m, int n, int[][] m1) {
        M = m;
        N = n;
        this.m = m1;
    }
}

public class Variant14 {

    public static Circle R_S(double L){
        Circle c = new Circle(0,0);
        c.r =L/(3.14*2);
        c.s = c.r*c.r*3.14;
        return c;
    }

    /******************************************************************************************************************/

    public static int number(int n){
        //System.out.println((n%10)*100+n/10);
        int a,b,c;
        a = n / 100;
        b = n / 10 - a * 10;
        c = n % 10;
        return (c * 100 + b * 10 + a);
    }

    /******************************************************************************************************************/

    public static boolean isTrue(int A, int B, int C){
        return ((A>0 && B<0 && C<0) || (A<0 && B>0 && C<0) || (A<0 && B<0 && C>0));
    }

    /******************************************************************************************************************/

    public static MinMax MM(int A, int B, int C){
        MinMax m = new MinMax(0,0);
        if(A<B && A<C){
            if (B<C){
                //System.out.println(A+", "+C);
                m.min = A;
                m.max = C;
                return m;
            }else{
                //System.out.println(A+", "+B);
                m.min = A;
                m.max = B;
                return m;
            }
        }else
            if(B<A && B<C){
                if (A<C){
                    //System.out.println(B+", "+C);
                    m.min = B;
                    m.max = C;
                    return m;
                }else{
                    //System.out.println(B+", "+A);
                    m.min = B;
                    m.max = A;
                    return m;
                }
            }else{
                    if (A<B){
                        //System.out.println(C+", "+B);
                        m.min = C;
                        m.max = B;
                        return m;
                    }else{
                        //System.out.println(C+", "+A);
                        m.min = C;
                        m.max = A;
                        return m;
                    }
                }

    }

    /******************************************************************************************************************/

    public static T triangle(int n, double v){
        T t = new T(0,0,0,0);
        switch (n){
            case 1:{
                t.a = v;
                t.R1 = t.a * Math.sqrt(3) / 6;
                t.R2 = t.R1 * 2;
                t.s = t.a * t.a * Math.sqrt(3) / 4;
                break;
            }
            case 2:{
                t.a = v * 6 / Math.sqrt(3);
                t.R1 = v;
                t.R2 = 2 * v;
                t.s = t.a * t.a * Math.sqrt(3) / 4;
                break;
            }
            case 3:{
                t.a = v * (6 / 2) / Math.sqrt(3);
                t.R1 = v /2;
                t.R2 = v;
                t.s = t.a * t.a * Math.sqrt(3) / 4;
                break;
            }
            case 4:{
                t.a = Math.sqrt(v * 4 / Math.sqrt(3));
                t.R1 = t.a * Math.sqrt(3) / 6;
                t.R2 = t.R2 * 2;
                t.s = v;
                break;
            }
        }
        return t;
    }

    /******************************************************************************************************************/

    public static Pow2 pow2(int N){
        Pow2 p = new Pow2(0,null);
        p.N = N;
        p.sum = new int[N];
        int sum = 0;
        for (int i = 1; i <= N; ++i){
            sum += 2 * i - 1;
            p.sum[i-1] = sum;
            //System.out.println(sum);
        }
        return p;
    }

    /******************************************************************************************************************/

    public static Sum sum(double A){
        Sum s = new Sum(0,0);

        s.K = 0;
        s.sum = 0;

        while(s.sum + 1.0 / (s.K +1 ) < A) {
            ++s.K;
            s.sum += 1.0 / s.K;
        }
        //System.out.println(K);
        return s;
    }

    /******************************************************************************************************************/

    public static Array array(int N, int[] a){
        Array x = new Array(0,null);

        x.index = new LinkedList<>();

        for (int i = N-1; i >0 ; --i){
            if(a[i]>a[i-1]){
                x.count++;
                x.index.add(i);
                //System.out.println(i);
            }
        }
        return x;
    }

    /******************************************************************************************************************/

    /**
     *
     * @param matrix
     * @return
     */
    public static Matrix matrix(Matrix matrix){
        Matrix m = new Matrix(matrix.M,matrix.N,null);
        int t;
        m.m = new int[m.M][m.N];
        for (int i = 0; i < m.M; ++i){
            for (int j = 0; j < m.N; ++j){
                //t = m.m[i][j];
                m.m[i][j] = matrix.m[i][m.N-j-1];
                //m.m[m.N-i-1][j] = t;
            }
        }
        return m;
    }
}
