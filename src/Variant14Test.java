import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.LinkedList;

public class Variant14Test {

    @Test(dataProvider = "testR_SProvider")
    public void testR_S(double L, Circle res) {
        Assert.assertEquals(res.r, Variant14.R_S(L).r, 0);
        Assert.assertEquals(res.s, Variant14.R_S(L).s, 0);
    }

    @DataProvider
    public Object[][] testR_SProvider(){
        return new Object[][]{{6.28, new Circle(1,3.14)}, {12.56, new Circle(2,12.56)}, {0, new Circle(0,0)}};
    }

    /******************************************************************************************************************/

    @Test(dataProvider = "testNumberProvider")
    public void testNumber(int n, int res) {
        Assert.assertEquals(res, Variant14.number(n));
    }

    @DataProvider
    public Object[][] testNumberProvider(){
        return new Object[][]{{324, 423}, {667, 766}, {100, 1}};
    }

    /******************************************************************************************************************/

    @Test(dataProvider = "testIsTrueProvider")
    public void testIsTrue(int A, int B, int C, boolean res){
        Assert.assertEquals(res, Variant14.isTrue(A, B, C));
    }

    @DataProvider
    public Object[][] testIsTrueProvider(){
        return  new Object[][]{{1, -2, -3, true}, {1, 2, -3, false}};
    }

    /******************************************************************************************************************/

    @Test(dataProvider = "testMMProvider")
    public void testMM(int A, int B, int C, MinMax res){
        Assert.assertEquals(res.min, Variant14.MM(A,B,C).min);
        Assert.assertEquals(res.max, Variant14.MM(A,B,C).max);
    }

    @DataProvider
    public  Object[][] testMMProvider(){
        return new Object[][]{{1, 2, 3, new MinMax(1,3)}, {3, -20, 40, new MinMax(-20,40)}, {-1,2,-1, new MinMax(-1,2)}};
    }

    /******************************************************************************************************************/

    @Test(dataProvider = "testTriangleProvider")
    public void testTriangle(int n, double v, T res){
        Assert.assertEquals(res.a, Variant14.triangle(n,v).a, 0);
        Assert.assertEquals(res.R1, Variant14.triangle(n,v).R1, 0);
        Assert.assertEquals(res.R2, Variant14.triangle(n,v).R2, 0);
        Assert.assertEquals(res.s, Variant14.triangle(n,v).s, 0);
    }

    @DataProvider
    public  Object[][] testTriangleProvider(){
        return new Object[][]{{1, 10, new T(10,10*Math.sqrt(3)/6,10*Math.sqrt(3)/3,25*Math.sqrt(3))}, {2, 10*Math.sqrt(3)/6, new T(10,10*Math.sqrt(3)/6,10*Math.sqrt(3)/3,25*Math.sqrt(3))}};
    }

    /******************************************************************************************************************/

    @Test(dataProvider = "testPow2Provider")
    public void testPow2(int N, Pow2 res){
        Assert.assertEquals(res.N, Variant14.pow2(N).N);
        Assert.assertEquals(res.sum, Variant14.pow2(N).sum);
    }

    @DataProvider
    public  Object[][] testPow2Provider(){
        return new Object[][]{{2, new Pow2(2,new int[]{1,4})}, {5, new Pow2(5,new int[]{1,4,9,16,25})}};
    }

    /******************************************************************************************************************/

    @Test(dataProvider = "testSumProvider")
    public void testSum(double A, Sum res){
        Assert.assertEquals(res.K, Variant14.sum(A).K);
        Assert.assertEquals(res.sum, Variant14.sum(A).sum, 0.01);
    }

    @DataProvider
    public  Object[][] testSumProvider(){
        return new Object[][]{{5, new Sum(82,4.99)}, {2, new Sum(3,1.83)}, {1.6, new Sum(2,1.5)}};
    }

    /******************************************************************************************************************/

    @Test(dataProvider = "testArrayProvider")
    public void testArray(int N, int[]a, Array res){
        Assert.assertEquals(res.count, Variant14.array(N,a).count);
        Assert.assertEquals(res.index, Variant14.array(N,a).index);
    }

    @DataProvider
    public  Object[][] testArrayProvider(){
        LinkedList<Integer>  a = new LinkedList<>();
        a.add(4);a.add(3);a.add(2);a.add(1);
        return new Object[][]{{5,new int[]{0,1,2,3,4}, new Array(a.size(),a)}};
    }

    /******************************************************************************************************************/

    @Test(dataProvider = "testMatrixProvider")
    public void testMatrix(Matrix M, Matrix res){
        Assert.assertEquals(res.M, Variant14.matrix(M).M);
        Assert.assertEquals(res.N, Variant14.matrix(M).N);
        for (int i = 0; i < res.M; ++i){
            for (int j = 0; j < M.N; ++j){
                Assert.assertEquals(res.m[i][j],Variant14.matrix(M).m[i][j]);
            }
        }
    }

    @DataProvider
    public  Object[][] testMatrixProvider(){
        return new Object[][]{{ new Matrix(2,4,new int[][]{{1,2,3,4},{4,3,2,1}}), new Matrix(2,4,new int[][]{{4,3,2,1},{1,2,3,4}}) }};
    }

    /******************************************************************************************************************/
}

